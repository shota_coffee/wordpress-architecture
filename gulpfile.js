'use strict'
var gulp = require('gulp');
var sass = require('gulp-sass');
var cssmin = require('gulp-cssmin');
var autoprefixer = require('gulp-autoprefixer');
var plumber = require('gulp-plumber');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;

gulp.task('sass', function() {
	gulp.src('./src/styles/**/*.scss')
	.pipe(sass())
	.pipe(autoprefixer())
	.pipe(cssmin())
	.pipe(plumber())
	.pipe(gulp.dest('./dist/styles/'))
})

gulp.task('sass-watch', ['sass'], function(){
  var watcher = gulp.watch('./src/styles/**/*.scss', ['sass']);
  watcher.on('change', function(event) {
  });
});

gulp.task('browserwatch', function() {
  browserSync.init({
    files: ['./**/*.php','./src/styles/*.scss'],
    proxy: 'http://localhost:8888/porta/',
  });
	 gulp.watch('./sass/**/*.scss', ['sass', reload]);
});

gulp.task('default', ['sass-watch','browserwatch']);
